package collections.array;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArrayListCustomTest {
    @Test
    public void removeFromEnd() {
        // Tutaj piszemy kod testu
        MyList stringList = new ArrayListCustom();
        stringList.add("Peter");
        stringList.add("John");
        stringList.add("Doe");
        String deleted = stringList.removeFromEnd();
        // powyzej wykonuje kod, ktory chce przetestowac

        //na koncu prosze JUNit o sprawdzenie
        // czy wykanane opercje zakonczyly sie
        // zgodnie z zalozeniami
        //metoda assertEquals -> sprawdza wynik testu
        // jezeli porownanie wynikow sie nie zgadza,
        // to test zakonczy sie niepowodzeniem
        // pierwszy parametr -> wpisujemy to czego oczekujemy
        // np. ze po usunieciu bede dwa elementu, lub, ze usuniety element to napis "Doe"
        // drugi paramter to fakryczna wartosc, ktora zwraca funkcja
        //oczekuje ze o usunieciu rozmiar bedzie 2 (1 parametr), i sprawdzam, czy faktycznie tak jest (drugi paramter)
        assertEquals(2, stringList.size());
        assertEquals("Doe", deleted);

    }

    // Test, w ktorym usuwamy z pustej listy
    // To jest sytacja wyjatkowa
    // wiec oczekujemy, ze skonczy sie wyrzuceniem wyjatku
    // nie umieszczamy bloku try catch
    // tylko informujemy JUNIT, ze oczekujemy tego wyjatku
    // i test ma sie zakonczyc powodzeniem, jesli taki wyjatek
    // zostanie rzucony
    // informujemy o tym fakcie podajac nazwe wyjatku
    // jako arugument adnotacji
    // w excpected przy wyjatku piszemy klase wyjatku jaka oczekujemy
    // a nie wartosc z funkcji
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void shouldGetErrorWhenListEmpty() {
        MyList stringList = new ArrayListCustom();
        stringList.removeFromEnd();
    }

    @Test
    public void shouldContainElement() {
        MyList list = new ArrayListCustom();
        list.add("Ala");
        boolean isElementPresent = list.contains("Ala");
        assertEquals(true, isElementPresent);
    }

    @Test
    public void shouldNotContainElement() {
        MyList list = new ArrayListCustom();
        boolean isPresent = list.contains("Ala");
        assertEquals(false, isPresent);
    }

    @Test
    public void shouldDeleteByIndex() {
        MyList list = new ArrayListCustom();
        list.add("Ala");
        list.add("ma");
        list.add("kota");
        list.add("test1");
        list.add("test2");
        list.add("test3");
        String removed = list.remove(2);

        assertEquals("kota", removed);
        assertEquals(5, list.size());


    }

    @Test
    public void shouldDeleteByName() {
        MyList list = new ArrayListCustom();
        list.add("Ala");
        list.add("ma");
        list.add("kota");
        list.add("test1");
        list.add("test2");
        list.add("test3");
        boolean result = list.remove("Ala");
        assertEquals(true, result);
        assertEquals(5, list.size());
    }

    @Test
    public void shouldAddInSelectedIndex() {
        MyList list = new ArrayListCustom(6);
        list.add("Ala");
        list.add("ma");
        list.add("kota");
        list.add("test1");
        list.add("test2");
        list.add("test3");
        list.add(2, "ELEMENT");
        assertEquals(7, list.size());
        assertEquals("ELEMENT", list.get(2));
    }


}