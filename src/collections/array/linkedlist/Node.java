package collections.array.linkedlist;

/**
 * Klasa reprezentujaca jeden wezel w liscie
 * Kazdy wezel listy (jednokierunkowej)
 * skada sie z pola danych i wskazania na nastepny element
 */
public class Node<E> {
    // pole przechowujace dane - typu generycznego
    private E value;
    // pole przechowujace referecje (wskazanie do kolejnego wezla)
    private Node<E> next;

    public Node(E value) {
        this.value = value;

    }

    public Node(E value, Node<E> next) {
        this.value = value;
        this.next = next;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }
}
