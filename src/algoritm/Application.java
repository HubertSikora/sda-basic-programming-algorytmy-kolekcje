package algoritm;

/**
 * Implementacja algorytmu znajdowania NWD wg Euklidesa
 */
public class Application {
    public static int NWD(int a, int b) {
        while (a != b) {
            if (a < b) {
                b = b - a;
            } else {
                a = a - b;
            }
        }
        return a;
    }

    public static void main(String[] args) {
        System.out.println(MathHelper.fibonacci(8));
    }
}
